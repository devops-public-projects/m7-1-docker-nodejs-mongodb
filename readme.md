# Docker Containers | NodeJS and MongoDB
This project will simulate a local development environment with Node.js and a Docker container for MongoDB that the app will leverage. We will also utilize a Docker container for MongoExpress to give us a UI for the MongoDB communication instead of only using a terminal.

## Technologies Used
- Docker
- Node.js
- MondoDB
- MongoExpress
- Git
- Linux (Ubuntu)


## Project Description
- Create a Dockerfile for a Node.js application and build a Docker image
- Run a Node.js application in a Docker container and connect it to a MongoDB database container locally
- Run a MongoExpress container as a UI of the MongoDB database

## Prerequisites
- Linux Machine configured with Docker, Git, Node.js, and NPM

## Guide Steps
### Initial Configurations
- Create project folder
	- `mkdir ~/repos/m7-1`
	- `cd ~/repos/m7-1`
- Download Sample Project
	- `git clone https://gitlab.com/twn-devops-bootcamp/latest/07-docker/js-app.git`
- Install Docker-cmpose
	- `sudo apt install docker-compose`

### Option 1 | Run Apps Separately
#### Docker
- Setup Docker
	- `docker network create mongo-network`
- [Setup MongoDB](https://hub.docker.com/_/mongo)
	- `docker pull mongo`
	- `docker run -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password –name mongodb –net mongo-network mongo`
- [Setup Mongo-Express](https://hub.docker.com/_/mongo-express)
	- `docker pull mongo-express`
	- `docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password --net mongo-network --name mongo-express -e ME_CONFIG_MONGODB_SERVER=mongodb mongo-express`

#### Mongo-Express Configuration
This could be configured using environment parameters but we will do this via the Mongo-Express UI for ease of use.
	- Web Browser: `localhost:8081`
	- Create Database
		- Name: `user-account`
	- Open `user-account` database
	- Create collection
		- Name: `users`

![Created user-account Database](/images/m7-1-create-database.png)

![Created users Collection](/images/m7-1-create-collection.png)


#### Run JS Application
- `cd ~/repos/m7-1\js-app\app`
- `nohup node server.js &`
- Web browser: `localhost:3000`
- If you make any changes using the `Edit Profile` button they will be saved in MongoDB

![JS App now running](/images/m7-1-js-app-now-running-1.png)

### Option 2 | Run Apps with Docker Compose
Note: If you previously ran the above commands to make a MongoDB and Mongo-Express container, you first need to `docker stop ID` them.
-  Build Project
	- `cd ~/repos/m7-1/js-app`
	- `docker build -t app .`
- Start the Docker containers for Mongo and Mongo-Express
	- `docker compose -f docker-compose.yaml up`
	- Mongo-Express UI for ease of use.
	- Web Browser: `localhost:8081`
	- Create Database
		- Name: `user-account`
	- Open `user-account` database
	- Create collection
		- Name: `users`
- Open the JS App
	- Web Browser: `localhost:3000`
	- If you make any changes using the `Edit Profile` button they will be saved in MongoDB

![JS App now running](/images/m7-1-js-app-now-running-2.png)

